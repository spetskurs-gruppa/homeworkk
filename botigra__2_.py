import json
import telebot
import types
import random

##This is IT-special course Homework. It's a Telegram Bot. link - https://t.me/oldmangamebot
##To compile this program you need to intstall pyTelegramBotAPI with pip
##The greeting is called with command /start
##If you write any text to it, it'll either think that it's a name(if l ==0) or a bet(if l == 1)'
##so yeah, it is just a little raw

bot = telebot.TeleBot('5257196019:AAHKVEF_a-XbvAPNxzWznnSoPi8w8g1kmZI')

def empty(): return None

def load_l():
	try:
		with open('users.json') as f:
			dan_users1 = json.loads(json.load(f))
	except:
		dan_users1 = {}
	return dan_users1

def po_balance(x):
		return x ['Balance']
		

x = 5500
y = 0
z = 0
l = 0
dan_users = load_l()
vrot = 'Деньги поставлены !'
rarkup = telebot.types.InlineKeyboardMarkup()
rarkup.add(telebot.types.InlineKeyboardButton(text = 'Начнем же игру !', callback_data = 'igra'))

@bot.message_handler(commands = ['start'])
def welcome_message(message):
	global dan_users
	if str(message.chat.id) in dan_users:
		privetstvie = 'Снова приветствую тебя, ' + dan_users [str(message.chat.id)] ['Name'] + '. Что желаешь сделать ?'
		fuarkup = telebot.types.InlineKeyboardMarkup()
		fuark1 = telebot.types.InlineKeyboardButton(text = 'Проверить кошель', callback_data = 3)
		fuark2 = telebot.types.InlineKeyboardButton(text = 'Сыграть в игру', callback_data = 4)
		fuark3 = telebot.types.InlineKeyboardButton(text = 'Посмотреть топ 10 игроков', callback_data = 21)
		fuarkup.row(fuark1,fuark2)
		fuarkup.row(fuark3)
		bot.send_message(message.chat.id, privetstvie, reply_markup = fuarkup)
	else:
		markup = telebot.types.InlineKeyboardMarkup()
		markup.add(telebot.types.InlineKeyboardButton(text = 'Да, хочу (Зарегистрироваться)', callback_data = 1))
		markup.add(telebot.types.InlineKeyboardButton(text = 'Расскажи мне побольше про эту игру', callback_data = 2))
		bot.send_message(message.chat.id, 'Здравствуй, путник, хочешь сыграть в игру 🃏?', reply_markup = markup)

@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
	
	global rarkup
	global vrot
	global x
	global y
	global z
	global dan_users
	
	varkup = telebot.types.InlineKeyboardMarkup()
	jom = telebot.types.InlineKeyboardButton(text = 'Играть снова', callback_data = 4)
	hom = telebot.types.InlineKeyboardButton(text = 'Проверить кошель', callback_data = 3)
	varkup.row(jom, hom)
	varkup3 = telebot.types.InlineKeyboardButton(text = 'Посмотреть топ 10 игроков', callback_data = 21)
	varkup.row(varkup3)
	bot.answer_callback_query(callback_query_id =call.id, text = 'Action done')
	answer = ' '
	if call.data == '1':
		answer = 'Отлично..Присаживайся и скажи мне своё имя'
		bot.send_message(call.message.chat.id, answer)
	if call.data == '2':
		answer = 'Игра очень проста: ты ставишь свои деньги на то,что угадаешь,где находится монетка, и если угадываешь, то сумма твоя удваивается. Однако если ты не угадаешь, я забираю все деньги себе.Ну что же, сыграем ?'
		makeup = telebot.types.InlineKeyboardMarkup()
		makeup.add(telebot.types.InlineKeyboardButton(text = 'Давай! ', callback_data = 1))
		bot.send_message(call.message.chat.id, answer, reply_markup = makeup)
	if call.data == '3':
		otv = 'В кошеле лежит ' + str(dan_users [str(call.message.chat.id)] ['Balance'])+ ' $'
		jarkup = telebot.types.InlineKeyboardMarkup()
		jarkup.add(telebot.types.InlineKeyboardButton(text = 'Показать их старцу(Перейти к ставкам)', callback_data = 4))
		bot.send_message(call.message.chat.id, otv, reply_markup = jarkup)
	if call.data == '4':
		vet = ' '
		garkup = telebot.types.InlineKeyboardMarkup(row_width = 2)
		a = telebot.types.InlineKeyboardButton(text = '10 $', callback_data = 5)
		b = telebot.types.InlineKeyboardButton(text = '50 $', callback_data = 6)
		c = telebot.types.InlineKeyboardButton(text = '100 $', callback_data = 7)
		d = telebot.types.InlineKeyboardButton(text = '200 $', callback_data = 8)
		e = telebot.types.InlineKeyboardButton(text = '300 $', callback_data = 9)
		f = telebot.types.InlineKeyboardButton(text = '400 $', callback_data = 10)
		g = telebot.types.InlineKeyboardButton(text = '500 $', callback_data = 11)
		h = telebot.types.InlineKeyboardButton(text = '1000 $', callback_data = 12)
		i = telebot.types.InlineKeyboardButton(text = '2000 $', callback_data = 13)
		k = telebot.types.InlineKeyboardButton(text = '3000 $', callback_data = 14)
		j = telebot.types.InlineKeyboardButton(text = '4000 $', callback_data = 15)
		l = telebot.types.InlineKeyboardButton(text = '5000 $', callback_data = 16)
		m = telebot.types.InlineKeyboardButton(text = 'Назову сумму сам', callback_data = 17)
		n = telebot.types.InlineKeyboardButton(text = 'Все мои деньги !', callback_data = 18)
		garkup.row(a, b, c, d, e, f)
		garkup.row(g,h,i,k,j,l)
		garkup.row(m, n)
		if dan_users [str(call.message.chat.id)] ['Balance'] != 0:
			if dan_users [str(call.message.chat.id)] ['Balance'] > 5500:
				vet = 'Ого, я смотрю ты уже подзаработал! Сколько хочешь поставить в этот раз ?'
			if dan_users [str(call.message.chat.id)] ['Balance'] == 5500:
				vet = 'Отлично, ровно столько, сколько нужно. Сколько хочешь поставить в этот раз ?'
			if dan_users [str(call.message.chat.id)] ['Balance'] < 5500 and x > 10:
				vet = 'Я думаю, этих денег тебе хватит.Сколько хочешь поставить в этот раз ?'
			bot.send_message(call.message.chat.id,vet, reply_markup = garkup)
		else:
			vet = 'Прости, но у тебя недостаточно денег для игры'
			bot.send_message(call.message.chat.id,vet)
	if call.data == '5':
		z = 10
		if dan_users [str(call.message.chat.id)] ['Balance'] >= z:
			hh = 'С баланса снялось '+ str(z)+ '$'
			dan_users [str(call.message.chat.id)] ['Balance'] -= z
			dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
			with open('users.json', 'w') as users_data1:
				json.dump(json.dumps(dan_users), users_data1)
			bot.send_message(call.message.chat.id, vrot, reply_markup = rarkup)
		else:
			hh = 'У вас недостаточно денег для совершения этой ставки, выберите другую сумму'
		bot.send_message(call.message.chat.id, hh)
		
	if call.data == '6':
		z = 50
		if dan_users [str(call.message.chat.id)] ['Balance'] >= z:
			aa = 'С баланса снялось '+ str(z)+ '$'
			dan_users [str(call.message.chat.id)] ['Balance'] -= z
			dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
			with open('users.json', 'w') as users_data2:
				json.dump(json.dumps(dan_users), users_data2)
			bot.send_message(call.message.chat.id, vrot, reply_markup = rarkup)
		else:
			aa = 'У вас недостаточно денег для совершения этой ставки, выберите другую сумму'
		bot.send_message(call.message.chat.id, aa)
		
	if call.data == '7':
		z = 100
		if dan_users [str(call.message.chat.id)] ['Balance'] >= z:
			bb = 'С баланса снялось '+ str(z)+ '$'
			dan_users [str(call.message.chat.id)] ['Balance'] -= z
			dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
			with open('users.json', 'w') as users_data3:
				json.dump(json.dumps(dan_users), users_data3)
			bot.send_message(call.message.chat.id, vrot, reply_markup = rarkup)
		else:
			bb = 'У вас недостаточно денег для совершения этой ставки, выберите другую сумму'
		bot.send_message(call.message.chat.id,bb)
		
	if call.data == '8':
		z = 200
		if dan_users [str(call.message.chat.id)] ['Balance'] >= z:
			cc = 'С баланса снялось '+ str(z)+ '$'
			dan_users [str(call.message.chat.id)] ['Balance'] -= z
			dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
			with open('users.json', 'w') as users_data4:
				json.dump(json.dumps(dan_users), users_data4)
			bot.send_message(call.message.chat.id, vrot, reply_markup = rarkup)
		else:
			cc = 'У вас недостаточно денег для совершения этой ставки, выберите другую сумму'
		bot.send_message(call.message.chat.id, cc)
		
	if call.data == '9':
		z = 300
		if dan_users [str(call.message.chat.id)] ['Balance'] >= z:
			dd = 'С баланса снялось '+ str(z)+ '$'
			dan_users [str(call.message.chat.id)] ['Balance'] -= z
			dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
			with open('users.json', 'w') as users_data5:
				json.dump(json.dumps(dan_users), users_data5)
			bot.send_message(call.message.chat.id, vrot, reply_markup = rarkup)
		else:
			dd = 'У вас недостаточно денег для совершения этой ставки, выберите другую сумму'
		bot.send_message(call.message.chat.id, dd)
		
	if call.data == '10':
		z = 400
		if dan_users [str(call.message.chat.id)] ['Balance'] >= z:
			ee = 'С баланса снялось '+ str(z)+ '$'
			dan_users [str(call.message.chat.id)] ['Balance'] -= z
			dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
			with open('users.json', 'w') as users_data6:
				json.dump(json.dumps(dan_users), users_data6)
			bot.send_message(call.message.chat.id, vrot, reply_markup = rarkup)
		else:
			ee = 'У вас недостаточно денег для совершения этой ставки, выберите другую сумму'
		bot.send_message(call.message.chat.id,
		ee)
		
	if call.data == '11':
		z = 500
		if dan_users [str(call.message.chat.id)] ['Balance'] >= z:
			ff = 'С баланса снялось '+ str(z)+ '$'
			dan_users [str(call.message.chat.id)] ['Balance'] -= z
			dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
			with open('users.json', 'w') as users_data7:
				json.dump(json.dumps(dan_users), users_data7)
			bot.send_message(call.message.chat.id, vrot, reply_markup = rarkup)
		else:
			ff = 'У вас недостаточно денег для совершения этой ставки, выберите другую сумму'
		bot.send_message(call.message.chat.id, ff)
		
	if call.data == '12':
		z = 1000
		if dan_users [str(call.message.chat.id)] ['Balance'] >= z:
			gg = 'С баланса снялось '+ str(z)+ '$'
			dan_users [str(call.message.chat.id)] ['Balance'] -= z
			dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
			with open('users.json', 'w') as users_data8:
				json.dump(json.dumps(dan_users), users_data8)
			bot.send_message(call.message.chat.id, vrot, reply_markup = rarkup)
		else:
			gg = 'У вас недостаточно денег для совершения этой ставки, выберите другую сумму'
		bot.send_message(call.message.chat.id, gg)
		
	if call.data == '13':
		z = 2000
		if dan_users [str(call.message.chat.id)] ['Balance'] >= z:
			ii = 'С баланса снялось '+ str(z)+ '$'
			dan_users [str(call.message.chat.id)] ['Balance'] -= z
			dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
			with open('users.json', 'w') as users_data9:
				json.dump(json.dumps(dan_users), users_data9)
			bot.send_message(call.message.chat.id, vrot, reply_markup = rarkup)
		else:
			ii = 'У вас недостаточно денег для совершения этой ставки, выберите другую сумму'
		bot.send_message(call.message.chat.id, ii)
		
	if call.data == '14':
		z = 3000
		if dan_users [str(call.message.chat.id)] ['Balance'] >= z:
			jj = 'С баланса снялось '+ str(z)+ '$'
			dan_users [str(call.message.chat.id)] ['Balance'] -= z
			dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
			with open('users.json', 'w') as users_data10:
				json.dump(json.dumps(dan_users), users_data10)
			bot.send_message(call.message.chat.id, vrot, reply_markup = rarkup)
		else:
			jj = 'У вас недостаточно денег для совершения этой ставки, выберите другую сумму'
		bot.send_message(call.message.chat.id, jj)
		
	if call.data == '15':
		z = 4000
		if dan_users [str(call.message.chat.id)] ['Balance'] >= z:
			kk = 'С баланса снялось '+ str(z)+ '$'
			dan_users [str(call.message.chat.id)] ['Balance'] -= z
			dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
			with open('users.json', 'w') as users_data11:
				json.dump(json.dumps(dan_users), users_data11)
			bot.send_message(call.message.chat.id, vrot, reply_markup = rarkup)
		else:
			kk = 'У вас недостаточно денег для совершения этой ставки, выберите другую сумму'
		bot.send_message(call.message.chat.id, kk)
		
	if call.data == '16':
		z = 5000
		if dan_users [str(call.message.chat.id)] ['Balance'] >= z:
			ll = 'С баланса снялось '+ str(z)+ '$'
			dan_users [str(call.message.chat.id)] ['Balance'] -= z
			dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
			with open('users.json', 'w') as users_data12:
				json.dump(json.dumps(dan_users), users_data12)
			bot.send_message(call.message.chat.id, vrot, reply_markup = rarkup)
		else:
			ll = 'У вас недостаточно денег для совершения этой ставки, выберите другую сумму'
		bot.send_message(call.message.chat.id, ll)
		
	if call.data == '17':
		bot.send_message(call.message.chat.id, 'Напишите сумму, которую хотите поставить')
	if call.data == '18':
		bot.send_message(call.message.chat.id,'Вау, ты смел и азартен')
		z = dan_users [str(call.message.chat.id)] ['Balance']
		oo = 'С баланса снялось '+ str(z)+ '$'
		dan_users [str(call.message.chat.id)] ['Balance'] -= z
		dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
		with open('users.json', 'w') as users_data13:
			json.dump(json.dumps(dan_users), users_data13)
		bot.send_message(call.message.chat.id, vrot, reply_markup = rarkup)
		bot.send_message(call.message.chat.id, oo)
		
	if call.data == 'igra':
		bot.send_message(call.message.chat.id, '*Старец крутит наперстки*')
		carkup = telebot.types.InlineKeyboardMarkup()
		nom1 = telebot.types.InlineKeyboardButton(text = 'Первый наперсток', callback_data = random.randint(19,20))
		nom2 = telebot.types.InlineKeyboardButton(text = 'Второй наперсток', callback_data = random.randint(19,20))
		nom3 = telebot.types.InlineKeyboardButton(text = 'Третий наперсток', callback_data = random.randint(19,20))
		carkup.row(nom1, nom2, nom3)
		bot.send_message(call.message.chat.id, "Выбери наперсток", reply_markup = carkup)
		
	if call.data == '19':
		bot.send_message(call.message.chat.id, '*Под наперстком пусто* Мне жаль, но ты проиграл. Хочешь сыграть снова ?', reply_markup = varkup)
		
	if call.data == '20':
		bot.send_message(call.message.chat.id,'*Под наперстком монетка* Что же, поздравляю, ты выиграл. Хочешь сыграть снова ?')
		dan_users [str(call.message.chat.id)] ['Balance'] += 2*z
		dan_users [str(call.message.chat.id)] ['Balance of old man'] -= 2*z
		with open('users.json', 'w') as users_data15:
			json.dump(json.dumps(dan_users), users_data15)
		sh = 'Баланс пополнился на ' + str(2*z) + '$'
		bot.send_message(call.message.chat.id, sh, reply_markup = varkup)
	if call.data == '21':
		to_r = list(dan_users.values())
		to_r.sort(key = po_balance, reverse = True)
		if len(to_r) > 10:
			for t in range(10):
				muss = str(t + 1) + '. ' + str(to_r [t] ['Name'])+ ' - Баланс: ' + str(to_r [t] ['Balance'])
				bot.send_message(call.message.chat.id, muss)
		else:
			for r in range(len(to_r)):
				muss = str(r+1) + '. ' + str(to_r [r] ['Name']) + ' - Баланс: ' + str(to_r [r] ['Balance'])
				bot.send_message(call.message.chat.id, muss)
		
		
@bot.message_handler(content_types = ['text'])
def text_handle(message):
	global x
	global y
	global z
	global l
	global vrot
	global rarkup
	global vpok
	global dan_users
	if l == 1:
		if any(map(str.isdigit, message.text)) and not(any(map(str.isalpha, message.text))):
			if int(message.text) > 10 and int(message.text) <= dan_users [str(message.chat.id)] ['Balance']:
				z = int(message.text)
				vpok = 'С баланса снялось '+ str(z)+ '$'
				dan_users [str(call.message.chat.id)] ['Balance'] -= z
				dan_users [str(call.message.chat.id)] ['Balance of old man'] += z
				with open('users.json', 'w') as users_data14:
					json.dump(json.dumps(dan_users), users_data14)
				bot.send_message(message.chat.id, vrot, reply_markup = rarkup)
				bot.send_message(message.chat.id, vpok)
			elif int(message.text) > 0:
				bot.send_message(message.chat.id, 'Введена сумма меньше минимальной ставки, напишите новую сумму')
			elif int(message.text) > dan_users [str(message.chat.id)] ['Balance']:
				bot.send_message(message.chat.id,'Вы не можете поставить больше, чем у вас есть, введите новую сумму')
			else:
				bot.send_message(message.chat.id, 'Ставка не может быть отрицательной, напишите новую сумму')
		else:
				bot.send_message(message.chat.id, 'Используйте цифры при написании суммы !')
			
	if l == 0:
		data_of_user = {str(message.chat.id): {'Name': message.text, 'Balance': x, 'Balance of old man': y}}
		dan_users.update(data_of_user)
		with open('users.json', 'w') as users_data:
			data = json.dump(json.dumps(dan_users), users_data)	
		tes = 'Хорошо, ' + message.text + ', теперь проверь свой кошель, там должны были остаться деньги'
		mukeup = telebot.types.InlineKeyboardMarkup()
		mukeup.add(telebot.types.InlineKeyboardButton(text = 'Проверить кошель', callback_data = 3))
		bot.send_message(message.chat.id, tes,reply_markup = mukeup)
		l += 1
		
		
bot.infinity_polling()